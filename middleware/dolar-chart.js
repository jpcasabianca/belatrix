export const planetChartData = {
  type: 'line',
  data: {
    labels: ['Example Date', 'Example Date', 'Example Date', 'Example Date', 'Example Date', 'Example Date', 'Example Date', 'Example Date', 'Example Date', 'Example Date', 'Example Date', 'Example Date', 'Example Date', 'Example Date', 'Example Date', 'Example Date', 'Example Date', 'Example Date', 'Example Date', 'Example Date'],
    datasets: [
      { // one line graph
        data: [8, 15, 12, 18, 21, 19, 25, 38, 35, 45, 60, 115, 105, 185, 260, 304, 400, 521, 432, 602],
        backgroundColor: [
          'rgba(54,73,93,.0)', // Blue
        ],
        borderColor: [
          '#09b3d0',
        ],
        borderWidth: 3
      }
    ]
  },
  options: {
				responsive: true,
				title: {
					display: false
				},
        legend: {
          display: false
        },
				tooltips: {
          display: false,
					mode: 'index',
					intersect: false,
				},
				hover: {
          display: false,
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: false,
						scaleLabel: {
							display: true,
							labelString: 'Month'
						}
					}],
					yAxes: [{
						display: false,
						scaleLabel: {
							display: true,
							labelString: 'Value'
						}
					}]
				}
			}
}

export default planetChartData;
