import Vuex from 'vuex';
import axios from 'axios';

const createStore = () => {
  return new Vuex.Store({
    state: {
      exchangeInfo: 0,
      exchangeBase: "",
      exchangeBaseRate: "",
      exchangeTarget: "",
      exchangeDate: "",
      exchangeBaseFee: 0.015,
      exchangeTargetFee: 0.012,
      exchangeRates: [],
      loading: true,
      currencies: [
        {
          'slug': 'USD',
          'symbol': '$'
        },
        {
          'slug': 'EUR',
          'symbol': '€'
        }
      ]
    },
    actions: {
      loadData( { commit }, payload ) {
        axios.get(payload.url).then((response) => {
          // console.log(response.data, this)
          commit('updateExchangeRate', response.data);
          commit('changeLoadingState', false);
          commit('set_exchange_variables');
        })
      }
    },
    mutations: {
      updateExchangeRate(state, exchangeInfo) {
        state.exchangeInfo = exchangeInfo;
      },
      changeLoadingState(state, loading) {
        state.loading = loading;
      },
      set_exchange_variables(state) {
        state.exchangeBase = state.exchangeInfo.base;
        state.exchangeDate = state.exchangeInfo.date;
        state.exchangeRates = state.exchangeInfo.rates;
        if(state.exchangeBase == 'USD') {
          state.exchangeTarget = 'EUR';
          state.exchangeBaseRate = state.exchangeRates.EUR;
        } else {
          state.exchangeTarget = 'USD';
          state.exchangeBaseRate = state.exchangeRates.USD;
        }
      },
      set_base_variables( state, payload ) {

      },
    },
  });
};

export default createStore
